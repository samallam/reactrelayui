import React from 'react';
import { ButtonToolbar, Button } from 'react-bootstrap';
import MTToolTip from './MTToolTip';
import MTTextField from './MTTextField';
import revalidator from 'revalidator';
import MTCheckbox from './MTCheckbox';
var {PropTypes} = React;

let details = {};
let childElements = {};
let schema = {
  properties: {
    FirstName: {
      type: 'string',
      maxLength: 50,
      format: 'text',
      required: true,
      allowEmpty: false
    },
    LastName: {
      type: 'string',
      maxLength: 50,
      required: true,
      allowEmpty: false
    },
    CitizenId: {
      type: 'string',
      maxLength: 10,
      required: true,
      allowEmpty: false
    },
    DateOfBirth: {
      type: 'string',
      format: 'date',
      maxLength: 255,
      required: true,
      allowEmpty: false
    },
    PostalCode: {
      type: 'string',
      maxLength: 5,
      required: true,
      allowEmpty: false
    },
    EmailAddress: {
      type: 'string',
      format: 'email',
      maxLength: 255,
      required: true,
      allowEmpty: false
    },
    ConfirmEmailAddress: {
      type: 'string',
      format: 'email',
      maxLength: 255,
      required: true,
      allowEmpty: false
    },
    notrobot: {
      type: 'boolean',
      required: true,
      allowEmpty: false
    },
    istenentcountry: {
      type: 'boolean',
      required: false,
      allowEmpty: true
    },
    MobilePhone: {
      type: 'string',
      pattern: '^\\d{3} \\d{7}$',
      maxLength: 11,
      required: false,
      allowEmpty: true
    }
  }
};
let inputObject = [
  {
    type: 'text',
    id: 'FirstName',
    lable: 'First Name',
    name: 'fname',
    errMsg: 'Enter Valid Name',
    infoMsg: 'Please Enter your first name as it appears on Metlife Policy',
    maxLength: '50',
    pattern: '^[A-z]+$'
  },
  {
    type: 'text',
    id: 'LastName',
    lable: 'Last Name',
    name: 'lname',
    errMsg: 'Enter Valid Name',
    infoMsg: 'Please Enter your last name as it appears on Metlife Policy',
    maxLength: '50',
    pattern: '^[A-z]+$'
  },
  {
    type: 'tel',
    id: 'CitizenId',
    lable: 'Social Security Number',
    name: 'ssn',
    errMsg: 'Enter Valid SSN',
    pattern: '^\\d{5}-\\d{4}$',
    infoMsg: 'Please enter your Citizen ID',
    placeholder: 'xxxxx-xxxx',
    maxLength: '10'
  },
  {
    type: 'date',
    id: 'DateOfBirth',
    lable: 'Data of Birth',
    name: 'dob',
    errMsg: 'Enter Valid Date',
    pattern: '^(19|20)\\d{2}[\/.-](0[1-9]|1[0-2])[-\/.](0[1-9]|1\\d|2\\d|3[01])$',
    min: '1980-01-01',
    max: '2080-01-01',
    infoMsg: 'Enter your birthdate  as it appears on Metlife Policy (within 1900 -1995)',
    placeholder: 'MM/DD/YYYY'
  },
  {
    type: 'tel',
    id: 'PostalCode',
    lable: 'Zip Code',
    name: 'postalcode',
    errMsg: 'Enter Valid Postal code',
    pattern: '\\d{5}$',
    infoMsg: 'Please enter your Postal Code',
    placeholder: 'xxxxx',
    maxLength: '5'
  },
  {
    type: 'email',
    id: 'EmailAddress',
    lable: 'Email Address',
    name: 'email',
    pattern: '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
    errMsg: 'Enter Valid Email ID',
    infoMsg: 'Please enter your email address. If possible, please use the email address that’s already associated with your MetLife account'
  },
  {
    type: 'email',
    id: 'ConfirmEmailAddress',
    lable: 'Confirm Email Address',
    name: 'confirmemail',
    pattern: '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
    errMsg: 'Enter Valid Email ID',
    infoMsg: 'Please confirm email'
  }];

class MTLoginForm extends React.Component {
    constructor(props) {
      super(props);
    }
    static propTypes = {
      onValidation: PropTypes.func
    }
    state = {
      status: ''
    }
    _getValue(value,key) {
      details[key] = value;
    }
    _registerChild(key,context) {
      childElements[key] = {context: context};
    }
    clearForm() {
      for (var key in childElements) {
        if (childElements[key].context.type === 'checkbox') {
          childElements[key].context.setState({isChecked: false});
        }else {
          childElements[key].context.setState({text: ''});
        }
      }
      document.getElementById('MTLoginForm').reset();
      details = {};
    }
   _submitDetails() {
     var result = revalidator.validate(details,schema);
     console.log(result);
     if (result.valid) {
       if (details['EmailAddress'] !== details['ConfirmEmailAddress']) {
         childElements['EmailAddress'].context.setState({status: 'has-error'});
         childElements['EmailAddress'].context.setState(
              {helpText: childElements['EmailAddress'].context.props.help});
         childElements['ConfirmEmailAddress'].context.setState({status: 'has-error'});
         childElements['ConfirmEmailAddress'].context.setState(
              {helpText: childElements['ConfirmEmailAddress'].context.props.help});
         return false;
       }
       this.props.onValidation(details);
     }else {
       for (var i = result.errors.length - 1; i >= 0; i--) {
         var key = result.errors[i].property;
         childElements[key].context.setState({status: 'has-error'});
         childElements[key].context.setState(
              {helpText: childElements[key].context.props.help});
       }
       console.log('Validation Failed');
     }
   }

    render() {
      return (
            <form id='MTLoginForm'
            className={this.props.className + ' ' + this.state.status}>
                {
                    this.props.inputObject.map(function (object) {
                      return (
                            <div key={object.id}>
                            <lable className=''>
                            <b>{object.lable}</b>&nbsp;
                            <MTToolTip message={object.infoMsg}/>
                            </lable>
                            <MTTextField id={object.id} type={object.type}
                               registerChild={this._registerChild.bind(this)}
                               name={object.name}
                               pattern={object.pattern}
                               placeholder= {object.placeholder}
                               help={object.errMsg}
                               maxLength={object.maxLength}
                               isRequired={object.isRequired || 'true'}
                               getValue={this._getValue.bind(this)}/>
                            </div>);
                    },this)
                }
                <div className=''>
                <lable><b>Mobile Phone</b>(Optional)&nbsp;
                <MTToolTip message='Please enter your Mobile Number'/>
                </lable>
                <MTTextField
                               className='txtCommonInput'
                               id='MobilePhone'
                               registerChild={this._registerChild.bind(this)}
                               type='text'
                               name='MobilePhone'
                               placeholder= 'xxx xxxxxxx'
                               pattern='^\d{3} \d{7}$'
                               help='Enter valid mobile no' maxLength='11'
                               isRequired='false'
                               getValue={this._getValue.bind(this)}/>
                </div>
                <div className='isHumanContainer'>
                <lable><b>Humans Only</b><span className='mobilenoCaption'>
                    We ask this question to protect this site form
                    automated attack and  keep your information secure</span>
                </lable>
                <MTCheckbox
                        id='notrobot'
                        registerChild={this._registerChild.bind(this)}
                        type='checkbox'
                        name='notrobot'
                        help='please confirm you are not a robot'
                        label='I am not a robot'
                        isRequired='true'
                        getValue={this._getValue.bind(this)}/>
                </div>
                  <ButtonToolbar className='btnRegistrationAction'>
                    <Button className='btnNegativeAction' bsSize='small'
                    type='reset'>Cancel</Button>
                    <Button className='btnPositiveAction' bsStyle='success'
                    bsSize='small' onClick={this._submitDetails.bind(this)}>NEXT
                    </Button>
                  </ButtonToolbar>
            </form>
        );
    }
}

let registration = {
  heading: {
    title: 'Registation is quick and easy.',
    caption: 'learn more about how can register.',
    details: 'Enter your identification and contact information below.',
    message: 'All fields are required unless noted'
  }
};
MTLoginForm.defaultProps = {
  className: 'registrationForm',
  pageTitle: 'Register to view your online accounts',
  url: 'http://9.118.90.50:8080/api/metlife/register',
  registration: registration,
  inputObject: inputObject
};
export default MTLoginForm;
