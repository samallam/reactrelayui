import React from 'react';
class PageTitle extends React.Component {
    constructor(props) {
      super(props);
    }
    render() {
      return (
            <div className='lblPageTitle'>
            {this.props.title}</div>
      );
    }
}
PageTitle.defaultProps = { title: 'Page Title'};
export default PageTitle;
