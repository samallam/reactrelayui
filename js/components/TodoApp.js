import AddTodoMutation from '../mutations/AddTodoMutation';
import RegistationPage from './RegistrationPage';

class TodoApp extends React.Component {
   constructor(props) {
     super(props);
   }

  _getRegistrationDetails(details) {
      Relay.Store.update(new AddTodoMutation({details, viewer: this.props.viewer}), {
      onSuccess: () => {;
        console.log(this.props.viewer.todos.edges.length);
        alert('Registation onSuccess');
      }
    });
  }

  render() {
    return (
      <RegistationPage getRegistrationDetails=
      {this._getRegistrationDetails.bind(this)}/>
    );
  }
}
export default Relay.createContainer(TodoApp, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        todos(first: 90071) {
          edges {
            node {
              id,
            },
          },
        },
        ${AddTodoMutation.getFragment('viewer')},
      }
    `,
  },
});
