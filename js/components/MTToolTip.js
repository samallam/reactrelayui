import React from 'react';
import { Popover, OverlayTrigger } from 'react-bootstrap';

class MTToolTip extends React.Component {
    constructor(props) {
      super(props);
    }
    _getToopTip() {
      return <div style={{height:200}}><Popover id='popover' placement="right" positionLeft={300} positionTop={300}>
                {this.props.message}
              </Popover></div>;
    }
    render() {
      return (
        <OverlayTrigger trigger={['hover', 'focus']}
         overlay={this._getToopTip()}>
         <span className="glyphicon glyphicon-info-sign"></span>
        </OverlayTrigger>
         );
    }

}
MTToolTip.defaultProps = { message: 'Register to view your online accounts'};
export default MTToolTip;
