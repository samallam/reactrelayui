
import { ButtonToolbar, OverlayTrigger, Popover } from 'react-bootstrap';
class MTPopover extends React.Component {
  render() {
    return (
        <OverlayTrigger container={this} trigger={['hover', 'focus']} placement="right" overlay={<Popover title="Popover top"><strong>Holy guacamole!</strong> Check this info.</Popover>}>
          <span className="glyphicon glyphicon-info-sign"></span>
        </OverlayTrigger>
    );
  }
}
export default MTPopover;