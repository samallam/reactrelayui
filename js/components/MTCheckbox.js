var {PropTypes} = React;

export default class MTCheckbox extends React.Component {
   static propTypes = {
     type: PropTypes.string.isRequired,
     id: PropTypes.string.isRequired,
     pattern: PropTypes.string,
     help: PropTypes.string,
     placeholder: PropTypes.string,
     getValue: PropTypes.func.isRequired
   }
  componentDidMount() {
    this.props.registerChild(this.props.id,this);
  }
  state = {
    isChecked: this.props.isChecked,
    status: '',
    helpText: ''
  };

  _handleCheckboxChange(e) {
    this.setState({isChecked: e.target.checked});
    if (e.target.checked) {
      this.props.getValue(e.target.checked,this.props.id);
      this.setState({status: ''});
      this.setState({helpText: ''});
    }else {
      this.setState({status: 'has-error'});
      this.setState({helpText: this.props.help});
    }
  }
  render() {
    return (
      <div className={this.state.status}>
        <div className='checkbox'>
          <label>
            <input type='checkbox' id={this.props.id}
            onChange={this._handleCheckboxChange.bind(this)}
            checked={this.state.isChecked}/>
            {this.props.label}</label>
            <span id='helpBlock' className='help-block'>
            {this.state.helpText}</span>
        </div>
      </div>
    );
  }
}
MTCheckbox.defaultProps = { isChecked: false };
