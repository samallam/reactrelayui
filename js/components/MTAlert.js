import React from 'react';
import { Modal, } from 'react-bootstrap';

class MTAlert extends React.Component {
    constructor(props) {
      super(props);
    }
    state = {
      showModal:false
    }
    close() {
      this.setState({ showModal: false });
    },

    open() {
      this.setState({ showModal: true });
    },
    render() {
      return (
          <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
          <Modal.Title>this.{props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <p>{this.props.message}</p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
        </div>
         );
    }

}
MTAlert.defaultProps = { message: 'Hi Alert', title: 'MT'};
export default MTAlert;
