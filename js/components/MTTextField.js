var {PropTypes} = React;

export default class MTTextField extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    pattern: PropTypes.string,
    help: PropTypes.string,
    maxLength: PropTypes.string,
    placeholder: PropTypes.string,
    isRequired: PropTypes.string,
    getValue: PropTypes.func.isRequired
  }
  componentDidMount() {
    this.props.registerChild(this.props.id,this);
  }
  state = {
    text: this.props.initialValue || '',
    status: '',
    helpText: ''
  };

  checkTextFieldStatus() {
    if (!this.state.text) {
      this.setState({status: 'has-error'});
      this.setState({helpText: this.props.help});
      return;
    }
    this.setState({status: '',helpText: ''});
  }
  validateInput() {
    if (this.props.isRequired === 'true' || this.props.isRequired === true) {
      if (!this.state.text) {
        this.setState({status: 'has-error'});
        this.setState({helpText: this.props.help});
        return false;
      }
    }
    if (this.props.pattern) {
      var pattern = new RegExp(this.props.pattern);
      if (pattern.test(this.state.text)) {
        return true;
      }else {
        this.setState({status: 'has-error'});
        this.setState({helpText: this.props.help});
        return false;
      }
    }
    return true;
  }
  _handleOnblur() {
    this.checkTextFieldStatus();
    if (this.validateInput()) {
      this.props.getValue(this.state.text,this.props.id);
    }
  }

  _handleChange(e) {
    this.setState({text: e.target.value});
    this.setState({status: '',helpText: ''});
    this.props.getValue(e.target.value,this.props.id);
  }

  render() {
    return (
      <div className={this.props.className + ' ' + this.state.status}>
        <label for={this.props.id}>{this.props.label}</label>
        <input id={this.props.id}
        type={this.props.type}
        className='form-control'
        pattern={this.props.pattern}
        onChange={this._handleChange.bind(this)}
        onBlur={this._handleOnblur.bind(this)}
        placeholder={this.props.placeholder}
        isRequired={this.props.isRequired}
        value={this.state.text} autoComplete='off'
        maxLength={this.props.maxLength}/>
        <span id='helpBlock' className='help-block'>
        {this.state.helpText}</span></div>
    );
  }
}
MTTextField.defaultProps = { className: 'form-group' };
