import React from 'react';

class RegistationDescription extends React.Component {
    constructor(props) {
      super(props);
    }
    render() {
      return (
              <div className='registerHeading'>
                <div className='registerHeadingTitle'>
                    {this.props.heading.title}</div>
                    <div className='registerHeadingCaption'>
                    {this.props.heading.caption}</div>
                    <div className='registerHeadingDetails'>
                    {this.props.heading.details}</div>
                    <div className='registerHeadingMessage'>
                    {this.props.heading.message}</div>
                </div>
        );
    }
}

let heading = {
  title: 'Registation is quick and easy.',
  caption: 'learn more about how can register.',
  details: 'Enter your identification and contact information below.',
  message: 'All fields are required unless noted'
};
RegistationDescription.defaultProps = { heading: heading};
export default RegistationDescription;
