import React from 'react';
import PageTitle from './PageTitle';
import RegistationDescription from './RegistationDescription';
import MTLoginForm from './MTLoginForm';
var {PropTypes} = React;
class RegistationPage extends React.Component {
    constructor(props) {
      super(props);
    }
    static propTypes = {
      getRegistrationDetails: PropTypes.func
    }
    _getgetValidatedDetails(values) {
      this.props.getRegistrationDetails(values);
    }

    render() {
      return (
            <div>
                <PageTitle title={this.props.title}/>
                <RegistationDescription heading={this.props.heading}/>
                <MTLoginForm onValidation=
                {this._getgetValidatedDetails.bind(this)}/>
            </div>
      );
    }

}
let heading = {
  title: 'Registation is quick and easy.',
  caption: 'learn more about how can register.',
  details: 'Enter your identification and contact information below.',
  message: 'All fields are required unless noted'
};
RegistationPage.defaultProps = {
  title: 'Register to view your online accounts',
  heading: heading
};
export default RegistationPage;
